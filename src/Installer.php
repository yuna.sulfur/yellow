<?php
namespace Sulfur\Yellow;

use Composer\Script\Event;

class Installer
{
	const CONFIG = 'yellow';

	protected static $config = [
		'dir' => 'public/vendor/yellow/'
	];


	public static function install(Event $event)
	{
		$extra = $event->getComposer()->getPackage()->getExtra();

		if(is_array($extra) && isset($extra[self::CONFIG]) && is_array($extra[self::CONFIG]) )  {
		  $config = array_merge(self::$config, $extra[self::CONFIG]);
		} else {
			$config = $extra[self::$config];
		}
		$from = __DIR__ . '/../resources/';
		$to = $config['dir'];


		self::removeFolder($to);
		self::copyFolder($from, $to);

		echo 'Installed Yellow';
    }


	protected static function removeFolder($path)
	{
		if (is_file($path)) {
			return @unlink($path);
		} elseif (is_dir($path)) {
			$scan = glob(rtrim($path,'/') . '/*');
			foreach($scan as $index => $path) {
				self::removeFolder($path);
			}
			return @rmdir($path);
		}
	}


	protected static function copyFolder($src, $dest)
	{
		$dir = opendir($src);
		@mkdir($dest);
		while(false !== ( $file = readdir($dir)) ) {
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($src . '/' . $file) ) {
					self::copyFolder($src . '/' . $file, $dest . '/' . $file);
				}
				else {
					copy($src . '/' . $file, $dest . '/' . $file);
				}
			}
		}
		closedir($dir);
	}
}